﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Documents;

namespace WorkerForm;
/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    public int Errors { get; private set; }

    public MainWindow() {
        InitializeComponent();
        BirthYearComboBox.ItemsSource = Enumerable.Range(1950, DateTime.Today.Year - 1950)
            .Cast<object>()
            .Reverse()
            .Prepend("Choose the year")
            .ToList();
        BirthYearComboBox.SelectedIndex = 0;

        EducationLevelComboBox.SelectedIndex = 0;

        JobPositionComboBox.SelectedIndex = 0;

        Dispatcher.InvokeAsync(async () => DataContext = await WorkerCollection.Load());
    }

    private void CanSave(object sender, CanExecuteRoutedEventArgs e) {
        e.CanExecute = Errors == 0;
        e.Handled = true;
    }

    private async void Save(object sender, ExecutedRoutedEventArgs e) {
        var worker = (Worker)WorkerForm.DataContext;
        var collection = (WorkerCollection)DataContext;

        collection.Workers.Add(worker);
        await collection.Save();

        WorkerForm.DataContext = new Worker();

        e.Handled = true;
    }

    private void HandleValidationError(object? sender, ValidationErrorEventArgs e) {
        if (e.Action == ValidationErrorEventAction.Added)
            Errors++;
        else
            Errors--;

        ErrorCounterRun?.GetBindingExpression(Run.TextProperty)?.UpdateTarget();
    }

    private void EditWorker(object sender, RoutedEventArgs e) {
        var worker = (Worker)((Button)sender).DataContext;
        WorkerForm.DataContext = worker;
    }

    private async void DeleteWorker(object sender, RoutedEventArgs e) {
        var worker = (Worker)((Button)sender).DataContext;
        var collection = (WorkerCollection)DataContext;

        collection.Workers.Remove(worker);
        await collection.Save();
    }
}