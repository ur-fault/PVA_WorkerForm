﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace WorkerForm;
public class WorkerCollection : INotifyPropertyChanged
{
    public ObservableCollection<Worker> Workers { get; set; }

    public WorkerCollection(List<Worker> workers) {
        Workers = new(workers);
    }

    public WorkerCollection() {
        Workers = new();
    }

    public static async Task<WorkerCollection> Load() {
        var workers =
            await JsonSerializer.DeserializeAsync<List<Worker>>(
                new MemoryStream(Encoding.UTF8.GetBytes(File.Exists("workers.json")
                    ? await File.ReadAllTextAsync("workers.json")
                    : "[]"))) ?? new();

        Debug.Print($"Loaded {workers.Count} workers");

        var collection = new WorkerCollection(workers);

        return collection;
    }

    public async Task Save() {
        await using StreamWriter stream = new StreamWriter("workers.json");
        await JsonSerializer.SerializeAsync(stream.BaseStream, Workers);
    }

    public event PropertyChangedEventHandler? PropertyChanged;
}
