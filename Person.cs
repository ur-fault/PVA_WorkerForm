﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.Json.Serialization;

namespace WorkerForm;

public class Person : INotifyPropertyChanged, IDataErrorInfo, IEquatable<Person>
{
    private string _firstName = string.Empty;
    private string _lastName = string.Empty;
    private int _birthYear;

    public Guid Id { get; private set; } = Guid.NewGuid();

    [JustLettersFilter]
    public string FirstName {
        get => _firstName;
        set {
            _firstName = value;
            OnChangedProperty();
        }
    }

    [NoWhitespaceFilter]
    [NotEmptyFilter]
    [JustLettersFilter]
    public string LastName {
        get => _lastName;
        set {
            _lastName = value;
            OnChangedProperty();
        }
    }

    [MinimalAgeFilter(18, valueIsYear: true)]
    public int BirthYear {
        get => _birthYear;
        set {
            _birthYear = value;
            OnChangedProperty();
        }
    }

    [JsonIgnore] public string Fullname => FirstName + " " + LastName;

    public event PropertyChangedEventHandler? PropertyChanged;

    protected void OnChangedProperty([CallerMemberName] string? name = null) {
        if (name == null) return;

        //MessageBox.Show($"{name} changed value to {this.GetType().GetProperty(name)?.GetValue(this)}");
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }

    [JsonIgnore]
    public string Error => "";

    public string? this[string columnName] {
        get {
            var value = GetType().GetProperty(columnName)?.GetValue(this);

            // Find all attributes that report errors
            var errors = GetType()
                .GetProperty(columnName)
                ?.GetCustomAttributes(typeof(ValueFilterAttribute),
                    true)
                .Cast<ValueFilterAttribute>()
                .Select(attr => attr.Check(value))
                .Where(error => error is not null)
                .Select(error => error!)
                .ToList();

            return (errors is null || errors.Count == 0 ? null : string.Join(", ", errors)) ?? string.Empty;
        }
    }

    public bool Equals(Person? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Id.Equals(other.Id);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((Person)obj);
    }

    public override int GetHashCode()
    {
        return Id.GetHashCode();
    }
}